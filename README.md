## WPezClasses: Give WP Snippets

__As inspired by the GiveWP article "10 Snippets from the Snippet Library to Level Up Your Give Development Skills"__

The article's (https://givewp.com/code-snippet-library-level-up/) approach is procedural. This is a little different.

_Note: This is __not__ a textbook example of an OOP single-responsibility class. No need to panic. No one is going to die._  

> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### Simple Example


Pseudo code-y


    $new = new ClassGiveWPSnippers();
    
    // 1. Give Total Progress Bar Custom Color
    $new->setColorTotalsProgress('#f5f5f5');
    
    // and now the hook...
    add_filter('give_totals_progress_color', [$new, 'giveTotalsProgressColor'];
    
    // 2. Custom Country List
    // 3. Custom Country Order
    $new=>setCountries('DE', 'AT', 'FR', 'CH');
    $new->setCountriesAlphabetical(true);
    $new->setCountriesTop(['FR', 'de']);
    
    // and now the hook...
    add_filter('give_countries', [$new, 'giveCountries']);
    
    // 6. Prevent Auto-login on Donor Creation
    // fyi - no set needed here since the default is to return false
    // just the hook...
    add_filter('give_log_user_in_on_register', [$new, 'giveLogUserInOnRegister']);
    
    // 9. Even Add-ons Can Have Snippets
    // fyi - no set needed since the default is to add the <script>...</script>
    // just the hook...
    add_action( 'give_donation_form_after_submit', [$new, 'giveDonationFormAfterSubmit' ] );
    
Yup! Your eyes aren't deceiving you. A lot less code. 


### FAQ

__1) Why?__

Why not? It's always quicker and ez-ier to configure rather than code. 


__2) Can I use this in my plugin or theme?__

Yes, but to be safe, please change the namespace. 


 __3) - I'm not a developer, can we hire you?__
 
Yes, that's always a possibility. If I'm available, and there's a good mutual fit. 



### HELPFUL LINKS

 - https://givewp.com
 
 
 - https://wordpress.org/plugins/give


 - https://givewp.com/code-snippet-library-level-up/
 

### TODO

- 5 - Email tag for multi-level label chosen during donation

- 7 - Add Additional Donor Meta - probably as its own class

- 8 - Make a Custom Give Shortcode Like This One - probably as its own plugin


### CHANGE LOG

- v0.0.2 - 18 April 2019
    - UPDATED: namespace

- v0.0.1 - 10 April 2019
    - Hey! Ho!! Let's go!!! 