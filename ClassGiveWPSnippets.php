<?php

namespace WPezSuite\WPezClasses\GiveWPSnippets;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

class ClassGiveWPSnippets {


	protected $_str_hex_color_totals_progress;
	protected $_arr_countries;
	protected $_bool_countries_blank_first;
	protected $_bool_countries_alphabetical;
	protected $_arr_countries_top;
	protected $_bool_log_user_in_on_register;
	protected $_bool_donation_form_after_submit;


	public function __construct() {

		$this->setPropertyDefaults();
	}


	protected function setPropertyDefaults() {

		$this->_str_hex_color_totals_progress   = '#ffffff'; // TODO - use plugin default
		$this->_arr_countries                   = false;
		$this->_bool_countries_blank_first      = true;
		$this->_bool_countries_alphabetical     = false;
		$this->_arr_countries_top               = false;
		$this->_bool_log_user_in_on_register    = false;
		$this->_bool_donation_form_after_submit = true;

	}

	// https://givewp.com/code-snippet-library-level-up/


	/**
	 *
	 * == 1. Give Total Progress Bar Custom Color:
	 *
	 */

	public function setColorTotalsProgress( $str_hex = false ) {

		return $this->setHex( '_str_hex_color_totals_progress', $str_hex );

	}


	/**
	 * filter: give_totals_progress_color
	 *
	 * @return mixed
	 */
	public function giveTotalsProgressColor() {

		return $this->_str_hex_color_totals_progress;
	}


	/**
	 *
	 * == 2. Custom Country List:
	 * == 3. Custom Country Order:
	 *
	 */

	/**
	 * @param array $arr
	 *
	 * @return bool
	 */
	public function setCountries( $arr = [] ) {

		// this array should only be the oountry abbreviations

		return $this->setArray( '_arr_countries', $arr );
	}

	public function setCountriesAlphabetical( $bool = false ) {

		return $this->setBool( '_bool_countries_alphabetical', $bool );

	}

	public function setCountriesTop( $arr =false ) {

		// this array should only be the oountry abbreviations

		return $this->setArray( '_arr_countries_top', $arr );
	}

	public function setCountriesAddBlank( $bool = true ) {

		return $this->setBool( '_bool_countries_blank_first', $bool );

	}


	/**
	 * filter = give_countries
	 *
	 * @param $arr_countries
	 *
	 * @return array
	 */
	public function giveCountries( $arr_countries ) {

		$arr_new = $arr_countries;

		// subset of all countries?
		if ( is_array( $this->_arr_countries ) ) {

			// we need to manipulate $_arr_give_counrtries a bit to use it with array_intersect_key(()

			// https://www.php.net/manual/en/function.array-flip.php
			$arr_temp = array_flip( $this->_arr_countries );
			// https://www.php.net/manual/en/function.array-change-key-case.php
			$arr_temp = array_change_key_case( $arr_temp, CASE_UPPER );
			// https://www.php.net/manual/en/function.array-intersect-key.php
			$arr_new = array_intersect_key( $arr_countries, $arr_temp );

		}

		// alpha?
		if ( $this->_bool_countries_alphabetical === true ) {
			natcasesort( $arr_new );
		}

		// shift some to top?
		if ( is_array( $this->_arr_countries_top ) ) {

			$arr_temp = [];
			foreach ( $this->_arr_countries_top as $str_abbrv ) {

				$str_abbrv = strtoupper( $str_abbrv );
				if ( isset( $arr_new[ $str_abbrv ] ) ) {

					$arr_temp[ $str_abbrv ] = $arr_new[ $str_abbrv ];
				}
			}
			// no unset needed, the array_merge will move tops to the top 
			$arr_new = array_merge( $arr_temp, $arr_new );
		}

		// blank on top
		if ( $this->_bool_countries_blank_first === true ) {
			$arr_new = array_merge( [ '' => '' ], $arr_new );
		}

		return $arr_new;
	}



	/**
	 *
	 * == 4. Adding Style to the terms checkbox - This was a CSS trick
	 *
	 */

	/**
	 *
	 * == 5. Email tag for multi-level label chosen during donation - TODO
	 *
	 */


	/**
	 *
	 * == 6. Prevent Auto-login on Donor Creation
	 *
	 */


	public function setLogUserInOnRegister( $bool = true ) {

		return $this->setBool( '_bool_log_user_in_on_register', $bool );

	}


	/**
	 * filter: give_log_user_in_on_register
	 *
	 * @return mixed
	 */
	public function giveLogUserInOnRegister() {

		return $this->_bool_log_user_in_on_register;

	}


	/**
	 *
	 * == 7. Add Additional Donor Meta - TODO probably as its own class)
	 * https://github.com/impress-org/give-snippet-library/blob/master/donor/add-donor-meta.php
	 *
	 */


	/**
	 *
	 * == 8. Make a Custom Give Shortcode Like This One - TODO probably as its own plugin
	 * https://github.com/impress-org/give-snippet-library/blob/master/custom-shortcodes/goal-achieved-form-list-shortcode.php
	 *
	 */


	/**
	 *
	 * == 9. Even Add-ons Can Have Snippets
	 *
	 */

	public function setDonationFormAfterSubmit( $bool = true ) {

		return $this->setBool( '_bool_donation_form_after_submit', $bool );

	}

	/**
	 * action: give_donation_form_after_submit
	 */
	public function giveDonationFormAfterSubmit() {

		if ( $this->_bool_donation_form_after_submit == true ) {
			echo '<script >';
			echo 'jQuery( "input[name=give_tributes_show_dedication][value=\'yes\']" ) . prop( "checked", true );';
			echo '</script >';
		}
	}


	/**
	 *
	 * == 10. When a Snippet is More than a Snippet - Not a snippet...Pass :)
	 *
	 */


	// =======================================================

	protected function setArray( $str_prop = false, $arr = false, $int_min_count = 0, $int_max_count = false ) {

		if ( property_exists( $this, $str_prop )
		     && is_array( $arr )
		     && count( $arr ) >= absint( $int_min_count )
		     && ( $int_max_count === false || count( $arr ) <= absint( $int_max_count ) ) ) {

			$this->$str_prop = $arr;

			return true;
		}

		return false;
	}

	protected function setBool( $str_prop = false, $bool = '', $arr_flags = [] ) {

		if ( property_exists( $this, $str_prop ) ) {

			// http://php.net/manual/en/filter.filters.validate.php
			if ( is_array( $arr_flags ) && filter_var( $bool, FILTER_VALIDATE_BOOLEAN, $arr_flags ) ) {

				$this->$str_prop = $bool;

				return true;

			} else {
				$this->$str_prop = (boolean) $bool;

				return true;
			}

		}
		return false;
	}

	protected function setHex( $str_prop = false, $str_hex = false, $str_prefix = '#' ) {

		// http://php.net/manual/en/filter.filters.validate.php
		if ( property_exists( $this, $str_prop ) ) {

			$str_hex = ltrim( $str_hex, '#' );

			if ( preg_match( '/([A-Fa-f0-9]{3}){1,2}$/', $str_hex ) ) {
				// TODO - prefix with # is it doesn't exist
				$this->$str_prop = $str_prefix . $str_hex;

				return true;
			}
		}

		return false;
	}


}